#Installation of web server package
class webserver::install inherits webserver::params {
  package { $webserver:
    ensure => installed,
  }
}

