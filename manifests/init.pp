class webserver {
  Class['webserver::install'] ~> Class['webserver::config'] ~> Class['webserver::service'] ~> Class['webserver::files']
  include webserver::params, webserver::install, webserver::config, webserver::service, webserver::files
}

