#Activation of web server
class webserver::service inherits webserver::params {
  service { $webserver:
    name => $webserver,
    ensure => running,
    enable => true,
    hasrestart => true,
    require => Package[$webserver],
  }
}

