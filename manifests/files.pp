#Copy static index page
class webserver::files inherits webserver::params {
  file { '/var/www/html/initial.html':
    ensure => file,
    source => $file_index,
  }
}

