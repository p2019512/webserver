#Test version and define variable
class webserver::params {
  $file_vhost = "/root/webserver/files/initial.conf"
  $file_index = "/root/webserver/files/initial.html"

  if($operatingsystem=="Debian") or ($operatingsystem=="Ubuntu") {
    $webserver = "apache2"
    $vhost = "/etc/apache2/sites-available/initial.conf"
  }elsif($operatingsystem=="CentOS") {
    $webserver = "httpd"
    $vhost = "/etc/httpd/conf.d/initial.conf"
  }
}

