#Delete all default vHost
class webserver::config inherits webserver::params {
  if($operatingsystem=="Debian") or ($operatingsystem=="Ubuntu") {
    exec { 'Delete vHost':
      command  => '/bin/rm /etc/apache2/sites-*/*.conf',
      refreshonly => true,
    }
  }elsif($operatingsystem=="CentOS") {
    exec { 'Delete vHost':
      command  => '/bin/rm /etc/httpd/conf.d/*.conf',
      refreshonly => true,
    }
  }

#Copy and activate vHost
  file { $vhost:
    ensure => file,
    source => $file_vhost,
    subscribe => Exec['Delete vHost'],
  }

  if($operatingsystem=="Debian") or ($operatingsystem=="Ubuntu") {
    exec { 'Activate initial.conf':
      command  => '/usr/sbin/a2ensite initial.conf',
      notify   => Service[$webserver],
      subscribe => File[$vhost],
    }
  }elsif($operatingsystem=="CentOS") {
    exec { 'Activate initial.conf':
      command  => '/bin/echo "OK"',
      notify   => Service[$webserver],
      subscribe => File[$vhost],
    }
  }
}

