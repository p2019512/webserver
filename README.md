# Welcome to the Apache2 module

This module allows only Apache2 webserver installation on an empty VM with a Git repository. This document is usefull to guide you for the installation of this module and in order to light some points.




# Module Deployment
## Requirements
The Apache2 module requires to be installed on an empty VM.

For deployment of this module, it's necessary to use the commands below :

Debian : 
```
apt update 
apt install puppet 
```

CentOS :
```
yum update
yum install puppet 
```

Clone the git repository under root user at the root path :
```
git clone git@forge.univ-lyon1.fr:p2019512/webserver.git
```

You have to obtain this following tree :
```
root
`-- webserver
	|-- README.md
	|-- files
	| 	|-- initial.conf
	| 	`-- initial.html
	`-- manifests
		|-- config.pp
		|-- files.pp
		|-- init.pp
		|-- install.pp
		|-- params.pp
		`-- service.pp
```

Launch the module execution with the following command in root :
``` 
puppet apply --modulepath=/root/ -e "include webserver"
```


# Information about the module

## Repositories

The "Manifest" repository contains six files necessary for Apache2 service installation whose Puppet codes.
The "Files" repository contains the configuration files of the Apache2 webserver and the default page.

## Files

File "init.pp" contains the puppet modules and classes.
File "params.pp" check the VM version where we are and defines variables with wich we have to work.
File "install.pp" installs Apache2 webserver.
File "config.pp" allows to delete all default vHost, to copy, and to activate new vHost.
File "service.pp" matches to necessary commands to start the webserver.
File "files.pp" allows to copy the HTML index page of webserver.

<br/>

---

<br/>

# Bienvenue sur le module Apache2

Ce module sert uniquement à l'installation du serveur Web Apache2 sur votre machine virtuelle à l'aide d'un dépôt distant. Ce document est  utile pour vous guider lors de l'installation de ce module et pour éclaircir certains points.




# Déploiement du module 
## Prérequis
Le module Apache2 nécessite d'être installé sur une machine vierge.

Pour le déploiement du module il est nécessaire de faire les commandes suivantes :

Debian : 
```
apt update 
apt install puppet 
```

CentOS :
```
yum update
yum install puppet 
```

Clonez le repo git sous l'utilisateur root à la racine :
```
git clone git@forge.univ-lyon1.fr:p2019512/webserver.git
```

Vous devez obtenir l'arborescence suivante :
```
root
`-- webserver
	|-- README.md
	|-- files
	| 	|-- initial.conf
	| 	`-- initial.html
	`-- manifests
		|-- config.pp
		|-- files.pp
		|-- init.pp
		|-- install.pp
		|-- params.pp
		`-- service.pp
```

Lancer l'execution du module via la commande suivante en root :
``` 
puppet apply --modulepath=/root/ -e "include webserver"
```


# Information sur le module

## Les répertoires 

Le répertoire "Manifest" contient les six fichiers nécessaires à l'installation du service web Apache2 dont les codes Puppet.
Le répertoire "Files" contient le fichier de configuration du serveur web Apache2 ainsi que la page par défaut.

## Les fichiers

Le fichier "init.pp" contient les modules et class puppet.
Le fichier "params.pp" teste la version de la machine sur laquelle on se situe et définie les variables avec lesquelles nous allons travailler.
Le fichier "install.pp" fait l'installation du serveur web Apache2.
Le fichier "config.pp" permet de supprimer tous les vHost par défaut, de copier, et d'activer le nouveau vHost.
Le fichier "service.pp" correspond aux commandes nécessaires pour le démarrage du serveur web.
Le fichier "files.pp" permet de copier la page d'index html du serveur web.
